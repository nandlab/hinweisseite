<?php
$address = "255.255.255.255";
$default_port = 40000;

function SendUDP($port, $msg) {
	global $address;
	$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
	if (!$sock) {
		echo "Failed to open UDP socket.\n";
	}
	else {
		$returnval = socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);
		if (!$returnval) {
			echo "Failed to enable broadcast mode.\n";
		}
		else {
			echo "Broadcast mode enabled.\n";
			$returnval = socket_sendto($sock, $msg, strlen($msg), 0, $address, $port);
			socket_close($sock);
			if (!$returnval) {
				echo "Failed to send UDP packet.\n";
			}
			else {
				echo "UDP packet sent successfully.\n";
			}
		}
	}
}

function main() {
//	if ($_SERVER["REQUEST_METHOD"] != "POST") {
//		echo "Request method POST expected.\n";
//	}
//	else {
		global $default_port;
		$msg = $_REQUEST['value'];
		$port = $_REQUEST['port'];
		if (empty($port))
		{ $port = $default_port; }
		else {
			if (!is_numeric($port)) {
				echo "Port is not numeric.\n";
				return;
			}
			$port = intval($port);
		}
		if (!empty($msg))
		{ $msg = trim($msg); }
		if (empty($msg)) {
			echo "Nothing to send: value is either empty or consists only of whitespace.\n";
		}
		else {
			echo "Opening socket and sending packet...\n";
			SendUDP($port, $msg);
		}
//	}
}
main();
?>
