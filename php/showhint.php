<?php
$address = "255.255.255.255";
$port = "40000";

function escape_str_for_my_shell($str) {
	$trans = array("\\" => "\\\\", "\"" => "\\\"", "'" => "\\'");
	return strtr($str, $trans);
}

function send_showhint_cmd($hint) {
	global $address, $port;
	$hint_shellsafe = escape_str_for_my_shell($hint);
	$showhint_cmd = "showhint \"" . $hint_shellsafe . "\"";
	$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
	if (!$sock) {
		echo "Failed to open UDP socket.\n";
	}
	else {
		$returnval = socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);
		if (!$returnval) {
			echo "Failed to enable broadcast mode.\n";
		}
		else {
			$returnval = socket_sendto($sock, $showhint_cmd, strlen($showhint_cmd), 0, $address, $port);
			socket_close($sock);
			if (!$returnval) {
				echo "Failed to send command.\n";
			}
			else {
				echo "Command sent successfully.\n";
			}
		}
	}
}

function main() {
	$hint = $_REQUEST['hint'];
	if (!empty($hint))
	{ $hint = trim($hint); }
	if (empty($hint)) {
		echo "Nothing to send: hint is either empty or consists only of whitespace.\n";
	}
	else {
		echo "Sending showhint command over UDP...\n";
		send_showhint_cmd($hint);
	}
}

main();

?>
