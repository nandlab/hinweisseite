function buildButtons()
{
	var riddle = ['Rätsel 1', 'Rätsel 2', 'Rätsel 3', 'Rätsel 4', 'Rätsel 5', 'Rätsel 6', 'Rätsel 7', 'Rätsel 8',
			 'Rätsel 9', 'Rätsel 10', 'Rätsel 11', 'Rätsel 12', 'Rätsel 13', 'Rätsel 14', 'Rätsel 15', 'Rätsel 16'];
	var openButton = "";
	var closeButton = "<h1><u>Reset</u> der einzelnen Rätsel</h1>";
	var esp1 = 400;
	var esp2 = 401;
	for (i = 0; i < riddle.length; i++) {
		var j = i+1;
		var k = i-7;
		if(i<8){
			openButton+='<button onclick="sendUDPString(\''+esp1+' Open Relais '+j+'\')">'+riddle[i]+'</button>';
		}
		
		if(i>=8){
			openButton+='<button onclick="sendUDPString(\''+esp2+' Open Relais '+k+'\')">'+riddle[i]+'</button>';
		}
		
		if (j%4 == 0 && i != 0 && i != riddle.length-1){
			openButton+='<br />';
		}
		
	}
	$('div.riddles').html(openButton);
	
	
	for (i = 0; i < riddle.length; i++) {
		var j = i+1;
		var k = i-7;
		if(i<8){
			closeButton+='<button onclick="sendUDPString(\''+esp1+' Close Relais '+j+'\')">'+riddle[i]+'</button>';
		}
							
		if(i>=8){
			closeButton+='<button onclick="sendUDPString(\''+esp2+' Close Relais '+k+'\')">'+riddle[i]+'</button>';
		}
		
		if (j%8 == 0 && i != 0 && i != riddle.length-1){
			closeButton+='<br />';
		}
		
	}
	$('div.riddlesCloseBottom').html(closeButton);

}

function getHints(){
	var values = [];
        keys = Object.keys(localStorage);
		keys.sort();
		return keys;
}


function saveHint()
{
	var input = document.getElementById('hint1').value;
	keys = getHints();
	localStorage.setItem(input, input);
	buildDropdown();
}

function deleteHint()
{
	sel = document.getElementById('hintDropdown');
	localStorage.removeItem(sel[sel.selectedIndex].id);
	buildDropdown();
}


function buildDropdown()
{
	var str='<select id="hintDropdown">';
	keys = getHints();
	hintLength = keys.length;
	for (i=0; i < hintLength ; i++){
		if(!(keys[i].match(/commonHint.*/))){
			str+='<option value="'+localStorage.getItem(keys[i])+'" id="'+keys[i]+'">'+localStorage.getItem(keys[i])+'</option>';
		}
	}
	
	str+='</select>';
	str+='&nbsp;&nbsp;<img src="img/save.png" onclick="saveHint()" class="saveHint" />&nbsp;<img src="img/delete.png" onclick="deleteHint()" class="saveHint" />&nbsp;<img src="img/admin.png" onclick="showAdmin()" class="saveHint" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$('div.hintSaver').html(str);
}


function buildCommonHints()
{
	var str='<br/><center>';
	keys = getHints();
	hintLength = keys.length;
	for (i=0; i < hintLength ; i++){
		s = 'commonHint'+(i+1);
		if(!(localStorage.getItem(s)==null)){
			str+='<textarea readonly id="commonHint'+(i+1)+'" data-toggle="tooltip" title="Hinweis '+(i+1)+'">'+localStorage.getItem(s)+'</textarea>';
		}
	}
	str+='</center>';
	$('div.commonHints').html(str);
}

function showAdmin(){
	document.getElementById('outerWrapper').style.display = "none";
	document.getElementById('admin').style.display = "block";
}

function hideAdmin(){
	document.getElementById('outerWrapper').style.display = "flex";
	document.getElementById('admin').style.display = "none";
}

function saveCommonHint()
{
	var key = 'commonHint'+document.getElementById('savePlace').value;
	console.log(key);
	var input = document.getElementById('commonHint').value;
	console.log(input);
	if(localStorage.getItem(key)){
		if(confirm('Hinweis vorhanden. Überschreiben?')){
				localStorage.setItem(key, input);
		}
	}
	else {		
		localStorage.setItem(key, input);
	}
	buildCommonHints();
}
