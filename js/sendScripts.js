function sendUDPString(s, port)
{
	if(s.match(/.*Open.*/) && !confirm('Rätsel auslösen?')){
		return;
	}
	if(s.match(/.*Close.*/) && !confirm('Rätsel zurücksetzen?')){
		return;
	}
	if(s.match(/4..Reset.*/) && !confirm('Mikrocontroller resetten?')){
		return;
	}
	if(s.match(/.*NotAus.*/) && !confirm('Alle Schlösser öffnen?')){
		return;
	}
	// Beim BSP
	// $.post("/SendUDP", { key: "", value: s } );
	// Beim Raspberry Pi
	let args = { "value" : s };
	if (typeof port !== 'undefined') {
		args['port'] = String(port);
	}
	$.post("/php/sendudp.php", args );
}

function sendCommonHint(s){
	if(confirm('Hinweis abschicken?')){
		// Hinweissound abspielen
		sendUDPString('/cue/501/start', 53535);

		// Hinweis anzeigen beim BSP.
		/*
			$.post('/SetValues', { "Hinweistext" : s } );
			$.post('/SendUDP', { key: "", value: 'R4Hinweis' } );
		*/
		// Hinweis anzeigen beim Raspberry Pi.
		/*
			let udpcmd = "showhint \"" + s + "\"";
			$.post('/SendUDP.php', { "value" : udpcmd });
		*/
		$.post("/php/showhint.php", { "hint" : s } );
	}
}

function setValues(){
	s = document.getElementById('hint1').value;
	$.post('/SetValues', { "Hinweistext" : s } );
	document.getElementById('hint1').value = s;
}
